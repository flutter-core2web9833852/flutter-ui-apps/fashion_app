import 'package:fashion_app_v1/HomePage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Detailpage extends StatefulWidget {
  const Detailpage({super.key});

  @override
  State createState() => _DetailpageState();
}

class _DetailpageState extends State {
  bool flagS = false;
  bool flagM = false;
  bool flagL = false;
  bool flagXL = false;
  bool flagXXL = false;

  size(index) {
    if (index == 1) {
      flagS = true;
      flagM = false;
      flagL = false;
      flagXL = false;
      flagXXL = false;
    } else if (index == 2) {
      flagS = false;
      flagM = true;
      flagL = false;
      flagXL = false;
      flagXXL = false;
    } else if (index == 3) {
      flagS = false;
      flagM = false;
      flagL = true;
      flagXL = false;
      flagXXL = false;
    } else if (index == 4) {
      flagS = false;
      flagM = false;
      flagL = false;
      flagXL = true;
      flagXXL = false;
    } else if (index == 5) {
      flagS = false;
      flagM = false;
      flagL = false;
      flagXL = false;
      flagXXL = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.all(30.0),
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
        Row(
          children: [
            IconButton(
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Homepage()));
                },
                icon: const Icon(
                  Icons.arrow_back_ios_new_rounded,
                  size: 30,
                )),
            const Spacer(),
            Text(
              "Details",
              style: GoogleFonts.imprima(
                  fontSize: 24, fontWeight: FontWeight.w500),
            ),
            const Spacer(),
            const Icon(
              Icons.turned_in_not_rounded,
              size: 30,
            )
          ],
        ),
        Image.asset("assets/images/detial.png"),
        Row(
          children: [
            Text(
              "Premium\nTagerine Shirt",
              style: GoogleFonts.imprima(
                  fontSize: 32, fontWeight: FontWeight.w600),
            ),
            const Spacer(),
            Image.asset(
              "assets/images/option1.png",
              scale: 0.8,
            ),
            const SizedBox(
              width: 10,
            ),
            Image.asset(
              "assets/images/option2.png",
              scale: 0.8,
            ),
            const SizedBox(
              width: 10,
            ),
            Image.asset(
              "assets/images/option3.png",
              scale: 0.8,
            ),
          ],
        ),
        Row(
          children: [
            Text(
              "Size",
              style: GoogleFonts.imprima(
                  fontSize: 26, fontWeight: FontWeight.w600),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                setState(() {
                  size(1);
                });
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: flagS ? Colors.black : Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "S",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        color: flagS ? Colors.white : Colors.black54),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  size(2);
                });
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: flagM ? Colors.black : Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "M",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: flagM ? Colors.white : Colors.black54),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  size(3);
                });
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: flagL ? Colors.black : Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "L",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: flagL ? Colors.white : Colors.black54),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  size(4);
                });
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: flagXL ? Colors.black : Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "XL",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: flagXL ? Colors.white : Colors.black54),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            GestureDetector(
              onTap: () {
                setState(() {
                  size(5);
                });
              },
              child: Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                  color: flagXXL ? Colors.black : Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    "XLL",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: flagXXL ? Colors.white : Colors.black54),
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
          ],
        ),
        Row(
          children: [
            Text(
              "\$299.99",
              style: GoogleFonts.imprima(
                  fontSize: 34, fontWeight: FontWeight.w600),
            ),
            Spacer(),
            Container(
              height: 60,
              width: 160,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: const Color.fromARGB(255, 255, 126, 27)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Add To Cart",
                    textAlign: TextAlign.center,
                    style: GoogleFonts.imprima(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                ],
              ),
            )
          ],
        )
      ]),
    ));
  }
}
