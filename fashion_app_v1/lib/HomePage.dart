import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:fashion_app_v1/DetaiPage.dart';
import 'package:fashion_app_v1/OrderPage.dart';

class Homepage extends StatefulWidget {
  const Homepage({super.key});

  @override
  State createState() => _HomepageState();
}

class _HomepageState extends State {
  int currentIndex = 0;

  bool flag1 = true;
  bool flag2 = false;
  bool flag3 = false;
  bool flag4 = false;
  bool flag5 = false;

  options(int index) {
    if (index == 2) {
      flag1 = false;
      flag3 = false;
      flag4 = false;
      flag5 = false;
      flag2 = true;
    } else if (index == 3) {
      flag1 = false;
      flag2 = false;
      flag4 = false;
      flag5 = false;
      flag3 = true;
    } else if (index == 4) {
      flag1 = false;
      flag2 = false;
      flag3 = false;
      flag5 = false;
      flag4 = true;
    } else if (index == 5) {
      flag1 = false;
      flag2 = false;
      flag3 = false;
      flag4 = false;
      flag5 = true;
    } else {
      flag1 = true;
      flag2 = false;
      flag3 = false;
      flag4 = false;
      flag5 = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding:
            const EdgeInsets.only(left: 30.0, right: 30, top: 50, bottom: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                const Icon(
                  Icons.menu_rounded,
                  size: 40,
                ),
                const Spacer(),
                IconButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> Orderpage()));
                  } ,
                  icon: Icon(
                    Icons.circle_outlined,
                    size: 40,
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(left: 0.0, right: 0),
              child: Text("Explore",
                  style: GoogleFonts.imprima(
                      fontSize: 40, fontWeight: FontWeight.w900)),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 0.0, right: 0),
              child: Text("Best trendy Collection!",
                  style: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.grey.shade600)),
            ),
            const SizedBox(
              height: 30,
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        options(1);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: flag1
                            ? const Color.fromARGB(255, 255, 126, 27)
                            : Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12.0, right: 12, top: 8, bottom: 8),
                        child: Text(
                          "All",
                          style: GoogleFonts.imprima(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: flag1 ? Colors.white : Colors.black),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        options(2);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: flag2
                            ? const Color.fromARGB(255, 255, 126, 27)
                            : Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12.0, right: 12, top: 8, bottom: 8),
                        child: Text(
                          "Men",
                          style: GoogleFonts.imprima(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: flag2 ? Colors.white : Colors.black),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        options(3);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: flag3
                            ? const Color.fromARGB(255, 255, 126, 27)
                            : Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12.0, right: 12, top: 8, bottom: 8),
                        child: Text(
                          "Women",
                          style: GoogleFonts.imprima(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: flag3 ? Colors.white : Colors.black),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        options(4);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        color: flag4
                            ? const Color.fromARGB(255, 255, 126, 27)
                            : Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12.0, right: 12, top: 8, bottom: 8),
                        child: Text(
                          "Kids",
                          style: GoogleFonts.imprima(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: flag4 ? Colors.white : Colors.black),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                  GestureDetector(
                    onTap: () {
                      setState(() {
                        options(5);
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: flag5
                            ? const Color.fromARGB(255, 255, 126, 27)
                            : Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 12.0, right: 12, top: 8, bottom: 8),
                        child: Text(
                          "Others",
                          style: GoogleFonts.imprima(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: flag5 ? Colors.white : Colors.black),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 12,
                  ),
                ],
              ),
            ),
            Row(
              children: [
                Column(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: 160,
                          width: 160,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/explore1.png"),
                                  scale: 0.9)),
                        ),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("\$124.50",
                                    style: GoogleFonts.imprima(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w900)),
                                const SizedBox(
                                  height: 0,
                                ),
                                Text("Tagerine Shirt",
                                    style: GoogleFonts.imprima(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade600)),
                              ],
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            const CircleAvatar(
                              radius: 16,
                              backgroundColor: Colors.black,
                              child: Icon(
                                Icons.shopping_bag_outlined,
                                color: Colors.white,
                                size: 20,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        Container(
                          height: 220,
                          width: 160,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/explore3.png"),
                                  scale: 0.9)),
                        ),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("\$220.50",
                                    style: GoogleFonts.imprima(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w900)),
                                const SizedBox(
                                  height: 0,
                                ),
                                Text("Tagerine Shirt",
                                    style: GoogleFonts.imprima(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade600)),
                              ],
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            const CircleAvatar(
                              radius: 16,
                              backgroundColor: Colors.black,
                              child: Icon(
                                size: 20,
                                Icons.shopping_bag_outlined,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  children: [
                    Column(
                      children: [
                        Container(
                          height: 220,
                          width: 160,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/explore4.png"),
                                  scale: 0.9)),
                        ),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("\$199.50",
                                    style: GoogleFonts.imprima(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w900)),
                                const SizedBox(
                                  height: 0,
                                ),
                                Text("Tagerine Shirt",
                                    style: GoogleFonts.imprima(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade600)),
                              ],
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            const Detailpage()));
                              },
                              child: const CircleAvatar(
                                radius: 16,
                                backgroundColor: Colors.black,
                                child: Icon(
                                  size: 20,
                                  Icons.shopping_bag_outlined,
                                  color: Colors.white,
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Column(
                      children: [
                        Container(
                          height: 160,
                          width: 160,
                          decoration: const BoxDecoration(
                              image: DecorationImage(
                                  image:
                                      AssetImage("assets/images/explore2.png"),
                                  scale: 0.9)),
                        ),
                        Row(
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("\$120.50",
                                    style: GoogleFonts.imprima(
                                        fontSize: 24,
                                        fontWeight: FontWeight.w900)),
                                const SizedBox(
                                  height: 0,
                                ),
                                Text("Tagerine Shirt",
                                    style: GoogleFonts.imprima(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey.shade600)),
                              ],
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            const CircleAvatar(
                              radius: 16,
                              backgroundColor: Colors.black,
                              child: Icon(
                                size: 20,
                                Icons.shopping_bag_outlined,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                )
              ],
            )
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        height: 80,
        onDestinationSelected: (int index) {
          setState(() {
            currentIndex = index;
          });
        },
        selectedIndex: currentIndex,
        destinations: [
          NavigationDestination(
              icon: IconButton(
                onPressed: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => Dashboard()));
                },
                icon: const Icon(Icons.home_rounded),
                color: const Color.fromARGB(255, 255, 126, 27),
              ),
              // icon: Icon(
              //   Icons.home_rounded,
              //   size: 30,
              //   color: Color.fromRGBO(245, 146, 69, 1),
              // ),
              selectedIcon: const Icon(Icons.home_rounded),
              label: "Home"),
          const NavigationDestination(
              icon: Icon(
                Icons.search_rounded,
                size: 30,
                color: Color.fromARGB(255, 255, 126, 27),
              ),
              label: "Search"),
          const NavigationDestination(
              icon: Icon(
                Icons.shopping_cart_rounded,
                size: 30,
                color: Color.fromARGB(255, 255, 126, 27),
              ),
              label: "Cart"),
          const NavigationDestination(
              icon: Icon(
                Icons.settings,
                size: 30,
                color: Color.fromARGB(255, 255, 126, 27),
              ),
              label: "Setting"),
        ],
      ),
    );
  }
}
