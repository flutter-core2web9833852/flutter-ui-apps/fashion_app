import 'package:flutter/material.dart';
import 'package:fashion_app_v1/OrderPage.dart';
import 'package:google_fonts/google_fonts.dart';

class Checkoutpage extends StatefulWidget {
  const Checkoutpage({super.key});

  @override
  State createState() => _OrderpageState();
}

class _OrderpageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30, top: 50, bottom: 30),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Orderpage()));
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios_new_rounded,
                      size: 30,
                    )),
                const SizedBox(
                  width: Checkbox.width * 5.0,
                ),
                Text(
                  "Checkout",
                  style: GoogleFonts.imprima(
                      fontSize: 24, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Delivery Address",
                  style: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.black54),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Image.asset(
                      "assets/images/map.png",
                      scale: 0.9,
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                        child: Text(
                      "25/3 Housing State, Sylhet",
                      style: GoogleFonts.imprima(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.black),
                    )),
                    Text(
                      "Change",
                      style: GoogleFonts.imprima(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    const Icon(Icons.access_time_rounded),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Delivered in next 7 days",
                      style: GoogleFonts.imprima(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.black87),
                    )
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                Text(
                  "Payment Method",
                  style: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: Colors.black54),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Image.asset(
                      "assets/images/americanexpress.png",
                      scale: 0.8,
                    ),
                    const Spacer(),
                    Image.asset(
                      "assets/images/applepay.png",
                      scale: 0.8,
                    ),
                    const Spacer(),
                    Image.asset(
                      "assets/images/mastercard.png",
                      scale: 0.8,
                    ),
                    const Spacer(),
                    Image.asset(
                      "assets/images/paypal.png",
                      scale: 0.8,
                    ),
                    const Spacer(),
                    Image.asset(
                      "assets/images/visa.png",
                      scale: 0.8,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Add Voucher",
                      style: GoogleFonts.imprima(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 40,
                ),
                RichText(
                    text: TextSpan(
                        style: GoogleFonts.imprima(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.black54),
                        children: <TextSpan>[
                      TextSpan(
                          text: "Note :",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.red)),
                      const TextSpan(
                        text: "Use your order id at the payment. Your id ",
                      ),
                      TextSpan(
                          text: "#12345 ",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Colors.black)),
                      const TextSpan(
                        text:
                            "if you forget to put we can't confirm the payment.",
                      ),
                    ]))
              ],
            ),
          ),
          const SizedBox(
            height: 0,
          ),
          const Divider(
            indent: 50,
            endIndent: 50,
            thickness: 1,
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Total Items (2)",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$420.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Standard Delivery",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$25.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Total Payment",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$445.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 60,
                width: 160,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: const Color.fromARGB(255, 255, 126, 27)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Pay Now",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.imprima(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                  ],
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
