import 'package:flutter/material.dart';
import 'package:fashion_app_v1/HomePage.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fashion_app_v1/CheckoutPage.dart';

class Orderpage extends StatefulWidget {
  const Orderpage({super.key});

  @override
  State createState() => _OrderpageState();
}

class _OrderpageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30, top: 50, bottom: 30),
            child: Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Homepage()));
                    },
                    icon: const Icon(
                      Icons.arrow_back_ios_new_rounded,
                      size: 30,
                    )),
                const SizedBox(
                  width: Checkbox.width * 5.5,
                ),
                Text(
                  "Cart",
                  style: GoogleFonts.imprima(
                      fontSize: 24, fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
                left: 30.0, right: 30, top: 10, bottom: 20),
            child: Text(
              "My Orders",
              style: GoogleFonts.imprima(
                  fontSize: 34, fontWeight: FontWeight.w600),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          Slidable(
              key: const ValueKey(0),
              endActionPane: ActionPane(
                  openThreshold: 0.3,
                  closeThreshold: 0.3,
                  motion: const DrawerMotion(),
                  children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 50,
                            width: 60,
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    bottomLeft: Radius.circular(30)),
                                color: Color.fromARGB(255, 255, 126, 27)),
                            child: const Icon(
                              Icons.delete_outline_rounded,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 50,
                            width: 60,
                            decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 255, 126, 27)),
                            child: const Icon(
                              Icons.favorite_border_rounded,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        )
                      ],
                    ))
                  ]),
              child: Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Row(
                  children: [
                    Image.asset("assets/images/myorder1.png"),
                    const SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Premium\nTagerine Shirt",
                          style: GoogleFonts.imprima(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Yellow",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87),
                        ),
                        Text(
                          "Size M",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87),
                        ),
                        Row(
                          children: [
                            Text(
                              "\$199.50",
                              style: GoogleFonts.imprima(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              width: 100,
                            ),
                            Text(
                              "1x",
                              style: GoogleFonts.imprima(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              )),
          SizedBox(
            height: 20,
          ),
          Slidable(
              key: const ValueKey(1),
              endActionPane: ActionPane(
                  openThreshold: 0.3,
                  closeThreshold: 0.3,
                  motion: const DrawerMotion(),
                  children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 50,
                            width: 60,
                            decoration: const BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30),
                                    bottomLeft: Radius.circular(30)),
                                color: Color.fromARGB(255, 255, 126, 27)),
                            child: const Icon(
                              Icons.delete_outline_rounded,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: 50,
                            width: 60,
                            decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 255, 126, 27)),
                            child: const Icon(
                              Icons.favorite_border_rounded,
                              color: Colors.white,
                              size: 30,
                            ),
                          ),
                        )
                      ],
                    ))
                  ]),
              child: Padding(
                padding: const EdgeInsets.only(left: 30.0),
                child: Row(
                  children: [
                    Image.asset(
                      "assets/images/explore3.png",
                      height: 140,
                    ),
                    const SizedBox(
                      width: 12,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "Leather\nTagerine Coart",
                          style: GoogleFonts.imprima(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                        const SizedBox(
                          height: 8,
                        ),
                        Text(
                          "Brown",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87),
                        ),
                        Text(
                          "Size L",
                          style: GoogleFonts.imprima(
                              fontSize: 16,
                              fontWeight: FontWeight.w400,
                              color: Colors.black87),
                        ),
                        Row(
                          children: [
                            Text(
                              "\$220.50",
                              style: GoogleFonts.imprima(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            ),
                            const SizedBox(
                              width: 100,
                            ),
                            Text(
                              "1x",
                              style: GoogleFonts.imprima(
                                  fontSize: 24, fontWeight: FontWeight.w600),
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              )),
          const SizedBox(
            height: 40,
          ),
          const Divider(
            indent: 50,
            endIndent: 50,
            thickness: 1,
          ),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Text(
                      "Total Items (2)",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$420.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Standard Delivery",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$25.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      "Total Payment",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w400,
                          color: Colors.black54),
                    ),
                    const Spacer(),
                    Text(
                      "\$445.00",
                      style: GoogleFonts.imprima(
                          fontSize: 22,
                          fontWeight: FontWeight.w600,
                          color: Colors.black),
                    )
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>Checkoutpage()));
                },
                child: Container(
                  height: 60,
                  width: 160,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: const Color.fromARGB(255, 255, 126, 27)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Checkout Now",
                        textAlign: TextAlign.center,
                        style: GoogleFonts.imprima(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                            color: Colors.white),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          )
        ],
      ),
    );
  }
}
